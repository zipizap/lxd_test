#!/usr/bin/env bash
# Paulo Aleixo Campos zipizap123@gmail.com
set -o errexit
set -o pipefail
set -o nounset
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__dbg_on_off=off  # on off
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
function error { echo "ERROR in ${BASH_LINENO[0]} : $@"; exit 1; }
function dbg { [[ "$__dbg_on_off" == "on" ]] || return; echo -e '\033[1;34m'"dbg $(date +%Y%m%d%H%M%S) ${BASH_LINENO[0]}\t: $@"'\033[0m';  }

# USAGE: show_usage_and_exit "Last line message"
function show_usage_and_exit {
  local LAST_MSG="${1:-}"
  cat <<EOT

  USAGE:
    $0 <aTargetAlias>    <anAnsibleHostToRedirectTargetAlias>

  DESCRIPTION:
    Updates <aTargetAlias> to redirect to a different <anAnsibleHostToRedirectTargetAlias>
    The <aTargetAlias> should already exist in the hosts file. If its new, it will not be 
    added, you have to add it manually

  EXAMPLES:
    # sets "lxcHost-targetY" ---RedirectedTo---> "lxcHostB"
    $0 lxcHost-targetY   lxcHostB           

    # sets "C-target900"     ---RedirectedTo---> "MyContainer2"
    $0 C-target900       MyContainer2       

EOT
  shw_info "${LAST_MSG}"
  exit 1
}


function main {
  local YYYYMMDDhhmmss=$(date +%Y%m%d%H%M%S)
  local HOSTS_FILE="${__dir}"/hosts
  local HOSTS_BCKP="${__dir}"/logs/hosts."${YYYYMMDDhhmmss}"

  # Check args
  local TARGET="${1:-}"
  local ANSIBLE_HOST="${2:-}"
  [[ "${TARGET}" ]] || { show_usage_and_exit "Missing <aTargetAlias>, aborting"; }
  [[ "${ANSIBLE_HOST}" ]] || { show_usage_and_exit "Missing <anAnsibleHostToRedirectTargetAlias>, aborting"; }
  egrep "^${TARGET} " "${HOSTS_FILE}" &>/dev/null || { show_usage_and_exit "Did not found <aTargetAlias>='${TARGET}' in '${HOSTS_FILE}', aborting"; }
  egrep "^${ANSIBLE_HOST}" "${HOSTS_FILE}" &>/dev/null || { show_usage_and_exit "Did not found <anAnsibleHostToRedirectTargetAlias>='${ANSIBLE_HOST}' in '${HOSTS_FILE}', aborting"; }

 
  # Update hosts
  cp -v "${HOSTS_FILE}"  "${HOSTS_BCKP}"
  sed "s/^${TARGET} .*/${TARGET}    ansible_host=${ANSIBLE_HOST}/g" -i "${HOSTS_FILE}"
  diff --suppress-common-lines "${HOSTS_BCKP}" "${HOSTS_FILE}"
  exit 0
}

main "$@"
