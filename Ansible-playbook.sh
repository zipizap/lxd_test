#!/usr/bin/env bash
PLAYBOOK_FILE="${@: -1}"
[[ -r $PLAYBOOK_FILE ]] || { echo "Usage: $0 [options] myPlaybook.yml"; exit 1; }
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
LOGFILE="./logs/$(date +%Y%m%d)/$(date +%Y%m%d%H%M%S).$PLAYBOOK_FILE.log"
echo -e "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
mkdir -p $(dirname $LOGFILE) &>/dev/null
date '+%F_%T'
echo ansible-playbook --diff $@
export ANSIBLE_FORCE_COLOR=true
time python ansible-playbook --diff "$@" | tee $LOGFILE
shw_info ">>> Log stored: $LOGFILE"

## NOTES
# vars_prompts dont work with this file setup:
#  - because of all the redirections in calling ansible from other execs
#  - workaround NOK: pass variables by command line using --extra-vars "version=1.2.3 otherVar='mee too'". NOK: passwords in arguments are not safe, get stored in history and log files. So NOK this workaround
#  - workaround OK: use ansible-playbook instead of Ansible-Playbook.sh, for these cases
